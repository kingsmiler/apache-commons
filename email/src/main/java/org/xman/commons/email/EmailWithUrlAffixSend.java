package org.xman.commons.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import java.net.MalformedURLException;
import java.net.URL;

public class EmailWithUrlAffixSend {

    public static void send() {
        MultiPartEmail email = new MultiPartEmail();
        email.setTLS(true); //是否TLS校验，，某些邮箱需要TLS安全校验，同理有SSL校验
        email.setDebug(false);
        //email.setSSL(true);

        email.setHostName("smtp.sina.com");
        email.setAuthenticator(new DefaultAuthenticator("java@sina.com", "******"));
        try {
            // Create the attachment
            EmailAttachment attachment = new EmailAttachment();
            attachment.setURL(new URL("http://www.apache.org/images/asf_logo_wide.gif"));
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Apache logo");
            attachment.setName("我的份");

            email.setFrom("java@sina.com"); //发送方
            email.addTo("java@gmail.com"); // 接收方
            //email.addCc("1234567@qq.com"); // 抄送方
            //email.addBcc("java@163.com"); // 秘密抄送方
            email.setCharset("GB2312");
            email.setSubject("有附件!!!"); // 标题
            email.setMsg("邮件发送测试。"); // 内容
            email.attach(attachment);
            email.send();
            System.out.println("发送成功");
        } catch (EmailException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        send();
    }
}
