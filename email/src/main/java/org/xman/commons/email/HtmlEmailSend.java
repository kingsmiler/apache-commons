package org.xman.commons.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import java.net.MalformedURLException;
import java.net.URL;

public class HtmlEmailSend {

    public static void send() {
        // Create the email message
        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.163.com");
        email.setTLS(true); //是否TLS校验，，某些邮箱需要TLS安全校验，同理有SSL校验
        email.setAuthenticator(new DefaultAuthenticator("java@163.com", "******"));
        try {
            email.setCharset("GB2312");
            email.addTo("java@gmail.com", "java");
            email.setFrom("java@163.com", "java");
            email.setSubject("内嵌图片背景测试");
            // embed the image and get the content id
            URL url = new URL("http://www.jianlimuban.com/resume/images/20081112155017201.jpg");
            String cid = email.embed(url, "Apache logo");
            // set the html message
            email.setHtmlMsg("<html>The apache logo - <img src='' ></html>");
            // 假如图片失效时显示的文字
            email.setTextMsg("Your email client does not support HTML messages");
            // send the email
            email.send();
            System.out.println("发送成功");
        } catch (EmailException |MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        send();
    }
}
