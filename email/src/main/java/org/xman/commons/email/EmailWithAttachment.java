package org.xman.commons.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

public class EmailWithAttachment {

    public static void send() {
        MultiPartEmail email = new MultiPartEmail();
        email.setDebug(true);

        //email.setTLS(true);
        //email.setSSL(true);

        email.setHostName("smtp.sina.com");
        email.setAuthenticator(new DefaultAuthenticator("java@sina.com", "******"));
        try {
            // Create the attachment
            EmailAttachment attachment = new EmailAttachment();
            //绝对路径
            attachment.setPath("F://dgjl.jpg");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Picture of John");
            attachment.setName("John");
            email.setFrom("java@sina.com"); //发送方
            email.addTo("java@gmail.com"); // 接收方
            //email.addCc("java@qq.com"); // 抄送方
            //email.addBcc("java@163.com"); // 秘密抄送方
            email.setCharset("GB2312");//编码
            email.setSubject("有附件!!!"); // 标题
            email.setMsg("邮件发送测试"); // 内容
            email.attach(attachment);
            email.send();
            System.out.println("发送成功");
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        send();
    }
}
